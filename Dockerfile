FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.17.0

COPY "wasm-pack-bin" "/usr/local/bin/wasm-pack"
